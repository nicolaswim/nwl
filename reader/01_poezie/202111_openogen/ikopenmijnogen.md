### 202111 Ik open mijn ogen

```{image} ogen.png
:alt: ogen
:class: bg-primary mb-1
:width: 500px
:align: center
```

```none
ik open mijn ogen
en ik heb al lang verloren
ik heb al uren gemist
ik spring op
vlieg doorheen de dag 
kijk om me heen
draai rondjes
scherp starend naar alles
beeld 1, 2, 3
zie je het niet? 
alles is pracht
alles is praal
sommige dingen doelloos
anderen met verhaal

hoe verder de dag reikt
hoe verder ik bereid ben op te nemen
ik wil me overgeven aan de wereld
zweven op de wolken van mijn zintuigen
geprikkeld sturen
ik wou dat het langer zou kunnen duren
constant op zoek naar een volgende steen om om te keren
wil ik blijven bijleren

de avond valt en de lichten doven uit
ik vlieg schichtig over straat 
paraat om eender welk moment te absorberen
ik ben nooit alleen durf ik te beweren

de stenen en het licht 
de huizen en hun gezicht 
de nacht en haar kleed
zij is degene die de dag ontleed
van bronnen en reflecties
tot zonnestralen en deflecties
is elke dag porselein servies
kieskeurig uitgeplukt
zodat het elke dag lukt
zodat elke dag weer verstopt zit met paasei-achtige cadeaus
in de vuilbak, onder mijn bureau 
verstopt in een ongeluk
in elke dag vind ik mijn geluk
```