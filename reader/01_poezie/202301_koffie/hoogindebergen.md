### Hoog in de bergen 

<!-- [![Alt text](hoogindebergen_youtube.jpg)](https://www.youtube.com/watch?v=sPnFVCIVdn4) -->

<iframe width="560" height="315" src="https://www.youtube.com/embed/sPnFVCIVdn4" frameborder="0" allowfullscreen></iframe>

```none
Als hoog in de bergen een masker schuilt
een geweer op stok 
maar klaar om te barsten
palmbladeren verzilten het zicht 
hier wordt waarde gesticht

bruin goud gebrand in barstende bomen
bommen blazen voor deze baronnen
beluister de brakke bramen
bekokstoof betwiste bekvechtende plannen
want in deze jungle worden bangerikken verbannen

dus grijp een wapen en bouw mee
voor hier wordt het Westerse geluk geoogst gezaaid en verbrand
zolang iemand maar de wacht houdt
wacht de rest aan de kant

dus de volgende keer 
dat de ochtendzon gluurt 
over een berg aan lakens en gemoedsrust
wees u dan bewust
waar de zwarte schoonheid vandaan komt
die gij elke ochtend zo graag kust
```