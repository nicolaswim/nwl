### Opstaan is

<!-- [![Alt text](opstaan_is_youtube.jpg)](https://www.youtube.com/watch?v=0mB6yKJ39Ro) -->

<iframe width="560" height="315" src="https://www.youtube.com/embed/0mB6yKJ39Ro" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

```none
### opstaan is ontdekken
ontdekken hoe mijn oogleden werken
terug uitvinden wat uit was gevallen
functies repatriëren en functionaliteiten recreëren 
voelen hoe dynamica vat heeft op mijn vaten
hoe mijn bloed valt in een gewichtig lichaam
dat atmosferische lichtheid is achterhaald
en plaats heeft gemaakt voor een mens dat is verdwaald

### opstaan is ontkenning
ontkennen dat wat nu nu is 
en de waarheid echt
dat kritiek verwezenlijkt is in een vorm
de dag, de zon, de macht
geen droom of verlangen
verwezenlijkt wroeten
nee roepen zonder stem
bereid zijn alles te laten vallen
voor een kleine sprong terug in de tijd
naar een moment zonder wallen

### opstaan is gewenning 
gewenning voor het daglicht
de sombere stralen regenen over mijn wimpers
ze weerspiegelen vochtige leden
ze dromen van een blik
ik kijk naar hun en zij kijken naar ik

### opstaan is baldadig
baldadig durven vechten tegen kou en winter
vechten tegen bezwete tenen en oververhitte lakens
achterlaten wat vannacht was
durven breken met een kort verleden
met een valse realiteit 
voor vandaag u in de wereld smijt

### opstaan is een kop koffie
vers gemalen en heet brouwend
een geur van wel vijf mijl ver
mijn slaap verheven
geen ontdekking, ontkenning, gewenning of baldadigheid
want koffie heeft mij bevrijdt 
geen wroeten en vegen, want koffie houdt mij vandaag in leven
verse kracht geserveerd in een kleine tasje
een espresso is het beste geschenk dat de natuur mij kon geven
```
