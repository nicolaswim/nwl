# 202305 De vooruitgang van de achteruitgang

<iframe width="560" height="315" src="https://www.youtube.com/embed/SrM0ka4-QQI?start=350" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

```
De lente is wedergekeerd
en daarmee de Belg ook
want ik daal op
van het zonnige Zuiden
om in dit koude bad
mijn tijd en tekst, en tête-à-tête, te betuigen
 
Rotterdam schijnt, zij straalt
van de singels en de grachten
van de plassen en de harten
van die oh zo schone 010
Ich habe noch nie eine solche Schönheit gesehen
 
Van die stellen met een bak en een fiets
een elektrische motor, en de auto?
die moet groter
 
de straten volgeplammuurd met Suzanne Bijl
volgeplakt met links aristocratische hypocrisie
de straten weggeveegd van smerige gastjes
de straten schoongemaakt van wiet en hasj
 
van     	gedrogeerde daklozen
            roekeloze jongeren
            kleinschalige drugsdealers
            en armzalige gastarbeiders
 
van     	hebt u nog 20 cent voor mij?
            hebt u even de tijd om te luisteren naar Jezus?
            wist u dat 1 op de 3 kinderen sterft?
            wil je mijn anti-honger repen kopen?
 
Welnu nee
ik ben niet wedergekeerd
om te luisteren naar narcissen
om te zien dat karakter plaats maakt voor een vale algemene standaardtaal
om te neigen naar gelikte tenten en modemodellen venten
om te nukken naar nalatige ingeënte necrofielen
 
Nee daar ben ik niet voor teruggekeerd
Wanneer kunnen we nog gewoon bestolen worden?
Verkracht en gebruikt in een steeg?
 
Je bril gebroken door een barbaar
je billen gebrand door je eigen blaren
een bebloede bal op je borst
en barrokaanse brok baldadigheid gebombardeerd op een stad die nog niet gebroken genoeg is
een besluitloos bestuur in de ban van beroemdheid
 
Ik pleit voor de vooruitgang van de achteruitgang
ik pleit voor boerka’s en bikini's
voor de objectificatie van materialisatie
voor het patroniseren van het matroniseren
voor de fiets die achteruit fietsen kan
voor de nieuwste zelfrijdende auto die helemaal van zelf
zonder ook maar één tik
zichzelf te pletter kan rijden
 
ik pleit voor een dag waar zelfs ik geen mening meer heb
ik pleit voor een Rotterdam, zonder hek
```
