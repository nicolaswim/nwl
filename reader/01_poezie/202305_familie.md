# 202305 Familie

<iframe width="560" height="315" src="https://www.youtube.com/embed/h8JMKDWpb-w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

```
het doorgeven van een sla kom 
het inschenken van de wijn 
de beschonken discussie 
de grote illusie 
dat de wereld stopt bij dit gezelschap 
en dat morgen niet meer verder schijnt
zonder het zijn van met elkaar
zonder het essentiële, zonder dat delen

dat is familie
familie is de eindeloze ruzie
want we zijn allemaal kwaad
omdat we allemaal niet hebben mogen kiezen

mijn broertje is de diepste vorm van liefde
ik zou gisteren, vandaag en morgen 
alles opgeven voor zijn zorgen

mijn zus is de grootste vorm van conflict
het grootste boegbeeld in mijn leven 
die mij o zo vaak uit de wind houdt 
en mij soms zo hard doet beven
want dat boegbeeld draait
en soms regelrecht naar mijn kop
en wanneer wij weer frontaal tegen elkaar staan
word ik zot 

het is dat samen snijden 
snoeiharde groentes
en zeemzoete desserts
doorgesauste pasta's 
en intrinsieke affaires

want familie
dat is mijn moeder
zij is en wij staan
wij zoeken en zij vindt
zij zucht en wij gaan
wij denken en zij begint

de geur van spanning
met een mogelijkheid tot conflict 
de aroma van resolutie 
met de kennis die verslindt

nauwkeurig uiteen gestippeld
van tip tot teen
dit gerecht mist geen steek

het zijn de handgebaren over tafel
het vragen om de olie of het geven van het zout
het is het dresseren van de sla
het schoonmaken van de schelpdieren 
waar mijn oma zo van houdt

want familie is een embleem
een eer om te dragen
een bodemloze put te behagen
edoch pleit ik voor hun
want zij zijn mij en ik ben hun 

wij vormen samen dit geheel
waar wij leren en vechten
goedmaken en hechten
aan een idee, aan een visie
aan de hoogste bergen van mijn bestaan
tot de diepste putten van een crisis
is mijn familie de oorzaak, het middel en de oplossing

dus voor hun 
deze kring

van woorden en emoties
van connaisaince en commotie 
voor mijn familie ben ik daar 
en mijn familie? 
die staat altijd klaar.
```
